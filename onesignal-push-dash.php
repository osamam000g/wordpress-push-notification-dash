<?php
    /**
     * Plugin Name: One Signal Push Dash 
     */
    function onesignal_push_dash(){
        register_post_type('Push Notifications', 
        array(
            "labels"=>array(
                "name"=>__("Push Notifications"),
                "add_new_item"=>_("Add New Push Notification")
            ),
            'menu_position'=>5,
            'public'=>true,
            'supports'=>array("title"),
            'exclude_from_search'=>true,
            'has_archive'=>false,
            'register_meta_box_cb'=>'os_show_meta_box',
            'show_in_rest'=>true,
        ));
    }
    add_action('init' , 'onesignal_push_dash');

    function os_show_meta_box(){
        // first arg id , title , call back fun , post type ,where to show the meta box normal or side , paiorty high low or leave blank 
        add_meta_box("os_meta_box_push" , "Push Notificaion Details" , "os_custom_fields_push" , "pushnotifications" , "normal" , "high" );
    }
    add_action('add_meta_boxes' , 'os_show_meta_box');


    
 




    function os_custom_fields_push(){
        global $post;
            $notificationTitle = get_post_meta($post->ID , "notification_title" , true);
            $notificationContent = get_post_meta($post->ID , "notification_content" , true);
            $notificationRes = get_post_meta($post->ID , "notification_response" , true);
            $notificationerr = get_post_meta($post->ID , "notification_err" , true);

            $os_onesignal_apikey1 = get_option('os_onesignal_apikey' , " ");
            $os_onesignal_appkey1 = get_option('os_onesignal_appkey' , " ");

            if (strlen($os_onesignal_apikey1) == 0 || strlen($os_onesignal_appkey1) == 0){
               ?>
               <div class="notice notice-warning is-dismissible">
          <p>Please enter the Api key and the app id .</p>
       </div>

                <?php
           }
        ?>
       
      
            <label>Notification Title</label>
            <input type="text" name="notification_title" placeholder="Title" class="widefat" value="<?php print $notificationTitle; ?>" /> 
            <br />
            <br />
            <label>Notification Content</label>
            <input type="text" name="notification_content" placeholder="Notification Content" class="widefat osTextArea" value="<?php print $notificationContent ; ?>" /> 

            <label>Notification response status</label>
            <input type="text" disabled name="notification_response" placeholder="Notification response" class="widefat osTextArea" value="<?php print $notificationRes ; ?>" /> 
            <!-- <label>Notification err</label>
            <input type="text" disabled name="notification_err" placeholder="Notification err" class="widefat osTextArea" value="<?php print $notificationerr ; ?>" />  -->

        <?php
    }

    function pushNotification_save($post_id){
        $is_autosave = wp_is_post_autosave($post_id);
        $is_revision = wp_is_post_revision($post_id);

        $os_onesignal_apikey2 = get_option('os_onesignal_apikey' , " ");
        $os_onesignal_appkey2 = get_option('os_onesignal_appkey' , " ");
        if($is_autosave || $is_revision){
                return;
        }
        $post = get_post($post_id);
        if($post->post_type == "pushnotifications"){
            if(array_key_exists('notification_title' , $_POST)){
                    update_post_meta($post_id , "notification_title" , $_POST['notification_title']);
                $titleos = $_POST['notification_title'];

            }
            if(array_key_exists('notification_content' , $_POST)){
                update_post_meta($post_id , "notification_content" , $_POST['notification_content']);
                $contenteos = $_POST['notification_content'];
                $response = wp_remote_post( 'https://onesignal.com/api/v1/notifications' , array(
                    'method' => 'POST',
                    'headers' => array('Content-Type'=>'application/json' , 'Authorization'=>'Basic '.$os_onesignal_apikey2),
                    'body' =>'{"app_id":"'.$os_onesignal_appkey2.'","included_segments": ["Subscribed Users"],"headings": {"en": "'.$titleos.'"},"contents": {"en": "'.$contenteos.'"}}',
                    )
                );
                
                if ( is_wp_error( $response ) ) {
                $error_message = $response->get_error_message();
            
                update_post_meta($post_id , "notification_response" ,"error in sending the notification");
                } else {
                    update_post_meta($post_id , "notification_response" ,"notification send successfully");
            
                }
            }
            

        }
    



    }
    add_action("save_post","pushNotification_save" );
    function os_push_notification_add_menu(){
    
        add_menu_page('one signal push notification from admin' , 'one signal settings' ,'manage_options' , 'onesignal_push_notification_from_admin' , 'page_inside_function_push_os' , '',200);    
    }

    add_action('admin_menu', 'os_push_notification_add_menu');

    function page_inside_function_push_os(){
        if(array_key_exists('os_submit_onesignal_settings',$_POST)){
                update_option('os_onesignal_apikey' , $_POST['os_onesignal_apikey']);
                update_option('os_onesignal_appkey' , $_POST['os_onesignal_appkey']);
                ?>
                    <div id="setting-error-settings-updated" class="updated-setting-error notice is-dissmissable">
                        <strong>
                            Settings have been saved
                        </strong>
                    </div>
                <?php
        }
        $os_onesignal_apikey = get_option('os_onesignal_apikey' , " ");
        $os_onesignal_appkey = get_option('os_onesignal_appkey' , " ");
        ?>
            <div class="wrap">
                <h2>Onesignal push notifications settings.</h2>
                <h3 class="updated-setting-error notice is-dissmissable"> Notice: To be able to use onesignal notification it requires SSL certificate.</h3>
                <form method="post" action="">
                    <label>onesignal API key</label>
                    <input type="text" name="os_onesignal_apikey" placeholder="API key" class="widefat osTextArea" value="<?php print $os_onesignal_apikey ; ?>" /> 

                    <label>One signal App id</label>
                    <input type="text"  name="os_onesignal_appkey" placeholder="App Key" class="widefat osTextArea" value="<?php print $os_onesignal_appkey ;?>" /> 
                    <br />
                    <br />
                    <input type="submit" name="os_submit_onesignal_settings" value="SAVE SETTINGS"  class="button button-primary" />
                </form>
            </div>
        <?php
    }
    function at_rest_init()
    {
        // route url: wp-json/alt/v1/notifications
        $namespaceAL = 'alt/v1';
        $routeAL     = 'notifications';
    
        register_rest_route($namespaceAL, $routeAL, array(
            'methods'   => 'GET',
            'callback'  => 'al_get_notifications'
        ));

        function al_get_notifications(){
        $notiArgs =[
            'numberposts' => 99999,
            'post_type' => 'pushnotifications'
        ];
        $notifiDataposts = get_posts($notiArgs);
        $data22 = []; 
        foreach($notifiDataposts as $notifiDatapost){
            $data22[$i]['id']       = $notifiDatapost->ID;
            $data22[$i]['title']    = $notifiDatapost->post_title;
            $data22[$i]['ntitle']   = get_post_meta($notifiDatapost->ID , "notification_title" , true);
            $data22[$i]['ncontent'] = get_post_meta($notifiDatapost->ID , "notification_content" , true);

$i++;


        }

            
            return new WP_REST_Response($data22);
        }
    }
    
    add_action('init', 'at_rest_init');

  
 

    ?>